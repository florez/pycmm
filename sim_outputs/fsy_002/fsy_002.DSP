



  Performance statistics for DSP solver: icalls =     22

 ===========================
 = multifrontal statistics =
 ===========================

     number of equations                     =            4524
     no. of nonzeroes in lower triangle of a =          118542
     no. of nonzeroes in the factor l        =          595270
     ratio of nonzeroes in factor (min/max)  =          1.0000
     number of super nodes                   =             236
     maximum order of a front matrix         =             306
     maximum size of a front matrix          =           46971
     maximum size of a front trapezoid       =           33768
     no. of floating point ops for factor    =      8.1923D+07
     no. of floating point ops for solve     =      2.1005D+06
     ratio of flops for factor (min/max)     =          1.0000
     ratio of allocated memory (max/min)     =          1.0000
     near zero pivot monitoring activated
     number of pivots adjusted               =               0
     negative pivot monitoring activated
     number of negative pivots encountered   =               0
     factorization panel size                =             128
     number of processes used                =               1
     number of threads per process used      =               2
     number of cores used                    =               2
     time (cpu & wall) for structure input   =        0.000000        0.000545
     time (cpu & wall) for ordering          =        0.001343        0.001343
     time (cpu & wall) for other matrix prep =        0.029907        0.001824
     time (cpu & wall) for value input       =        0.000000        0.001535
     time (cpu & wall) for matrix distrib.   =        0.000000        0.003815
     time (cpu & wall) for numeric factor    =        0.031250        0.009068
     computational rate (gflops) for factor  =        2.621522        9.033850

     no input or output performed

  Total Solver Memory allocated              =        9.573 MB
