FAILURE CHECK CMM-USERMAT
-------------------------------------------------------------------
-------------------------------------------------------------------
TIME STEP AT FAILURE       6.50000
-------------------------------------------------------------------
CONVERGENCE               0.0        0=conv., 1=no conv.             
CONCRETE FAILURE          0.0        0=no failure, 1=failure 
        
STEEL FAILURE             0.0        0=no failure, 1=failure 
        
SHEAR FAILURE             1.0        0=no failure, 1=failure 
        
                        449.0        failed element          
        
