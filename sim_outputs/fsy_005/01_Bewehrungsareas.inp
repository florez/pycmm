! ----------------------------------------------------------------------------
! ----------------------------------------------------------------------------
! 01 BEWEHRUNGSAREAS
! ----------------------------------------------------------------------------
! ----------------------------------------------------------------------------
!
! Bemerkung:    Das vorliegende Subfile erlaubt die Zuordnung von 
!               as_X_Lage, dm_X_Lage und psi_X_Lage eines Bewehrungsbereiches
!               zu den Ansys FL�chen �ber geometrische Gr�ssen. 
!               Das File ist aktuell nur f�r die X-Y Ebene geeignet.  
!               Ben�tigter Input: x1,x2,x3,x4,y1,y2,y3,y4 
!               (Nummerierung gegen Uhrzeigersinn)              
!
!-------------------------------------------------------------------------------
! 1.    ALLGEMEIN
!-------------------------------------------------------------------------------
!
! Bestimmung der minmalen/maximalen Ansys-Area Nummer in der Platte
asel,s,loc,z,0 
*GET,NrA_P,Area,0,COUNT               ! NrA_P: Anzahl Areas der Platte
*DIM,N_A_P,ARRAY,NrA_P,1
*VGET,N_A_P,AREA, ,ALIST              ! N_A: Auflistung der Fl�chen 
*vscfun,MIN_area_Number,min, N_A_P    ! Nummer Startfl�che f�r die Platte 
*vscfun,MAX_area_Number,max, N_A_P    ! Nummer Endfl�che f�r die Platte
allsel  
!
! Aufbau Vektor f�r Ansys-Areas n_F im gesamten Bauteil
*DIM,N_A,ARRAY,n_F,1
*VGET,N_A,AREA, ,ALIST                
!
! Aufbau der ben�tigten Arrays
*DIM,ALA,ARRAY,n_F,4
*DIM,d_LB_L1,ARRAY,n_F,n_BF
*DIM,d_LB_L1_check,ARRAY,n_F,n_BF
*DIM,d_LB_L2,ARRAY,n_F,n_BF
*DIM,d_LB_L2_check,ARRAY,n_F,n_BF
*DIM,d_LB_L3,ARRAY,n_F,n_BF
*DIM,d_LB_L3_check,ARRAY,n_F,n_BF
*DIM,d_LB_L4,ARRAY,n_F,n_BF
*DIM,d_LB_L4_check,ARRAY,n_F,n_BF
*DIM,ALA_BF,ARRAY,n_F,n_BF
*DIM,d_sum,ARRAY,n_F,n_BF
!
!-------------------------------------------------------------------------------
! 2.    Bestimmung der zu den Bew.fl�chen geh�renden Ansys Fl�che
!-------------------------------------------------------------------------------
!
*DO,ii,MIN_area_Number,MAX_area_Number  
asel,s,loc,z,0      
ASEL,s,AREA,,N_A(ii,1)           
!  
Asum                   
*GET,Xc,AREA,N_A(ii),CENT,X       ! X-coor abspeichern der aktuellen Ansys Fl. 
*GET,Yc,AREA,N_A(ii),CENT,Y       ! Y-coor abspeichern der aktuellen Ansys Fl. 
!
! Gesamtzahl der Bewehrungsfl�chen (Punkt-in-Polygon-Test nach Jordan) 
! (1=left side; 0 right side);   
*DO,iLB,1,n_BF 
!
  d_LB_L1(ii,iLB)=(Yc-y1(iLB))*(x2(iLB)-x1(iLB))-(Xc-x1(iLB))*(y2(iLB)-y1(iLB))
  *IF,d_LB_L1(ii,iLB),GE,0,THEN
      d_LB_L1_check(ii,iLB)=1
  *ELSE
      d_LB_L1_check(ii,iLB)=0
  *ENDIF
!
  d_LB_L2(ii,iLB)=(Yc-y2(iLB))*(x3(iLB)-x2(iLB))-(Xc-x2(iLB))*(y3(iLB)-y2(iLB))
  *IF,d_LB_L2(ii,iLB),GE,0,THEN
      d_LB_L2_check(ii,iLB)=1 
  *ELSE
      d_LB_L2_check(ii,iLB)=0 
  *ENDIF
!
  d_LB_L3(ii,iLB)=(Yc-y3(iLB))*(x4(iLB)-x3(iLB))-(Xc-x3(iLB))*(y4(iLB)-y3(iLB))
  *IF,d_LB_L3(ii,iLB),GE,0,THEN
      d_LB_L3_check(ii,iLB)=1 
  *ELSE
      d_LB_L3_check(ii,iLB)=0 
  *ENDIF
!
  d_LB_L4(ii,iLB)=(Yc-y4(iLB))*(x1(iLB)-x4(iLB))-(Xc-x4(iLB))*(y1(iLB)-y4(iLB)) 
  *IF,d_LB_L4(ii,iLB),GE,0,THEN
      d_LB_L4_check(ii,iLB)=1 
  *ELSE
      d_LB_L4_check(ii,iLB)=0 
  *ENDIF
!
  dA_sum=d_LB_L1_check(ii,iLB)
  dB_sum=d_LB_L2_check(ii,iLB)
  dC_sum=d_LB_L3_check(ii,iLB)
  dD_sum=d_LB_L4_check(ii,iLB)
  d_sum(ii,iLB)=dA_sum+dB_sum+dC_sum+dD_sum
!
  *IF,d_sum(ii,iLB),GE,3.9,THEN
      ALA_BF(ii,iLB)=N_A(ii) 
  *ELSE
  *ENDIF        
*ENDDO                
*ENDDO
!
asel,s,loc,z,0  
!
!-------------------------------------------------------------------------------
! 3.    Assemblierung as_X_Lage, dm_X_Lage, psi_X_Lage -> asX, dmX und psiX  
!-------------------------------------------------------------------------------
!
*DO,ii,MIN_area_Number,n_F   ! Schleife �ber alle Ansys Fl�chen (NrA)
  *DO,iLB,1,n_BF    ! Schleife �ber alle Bewehrungsfl�chen n_BF
!
    *IF,ALA_BF(ii,iLB),GT,0,THEN
        Asel,s,Area,,ALA_BF(ii,iLB)
        as1(ii) = as_1_Lage(iLB)
        as2(ii) = as_2_Lage(iLB)
        as3(ii) = as_3_Lage(iLB)
        as4(ii) = as_4_Lage(iLB)
        dm1(ii) = dm_1_Lage(iLB)
        dm2(ii) = dm_2_Lage(iLB)
        dm3(ii) = dm_3_Lage(iLB)
        dm4(ii) = dm_4_Lage(iLB)
        psi1(ii) = psi_1_Lage(iLB)
        psi2(ii) = psi_2_Lage(iLB)
        psi3(ii) = psi_3_Lage(iLB)
        psi4(ii) = psi_4_Lage(iLB)
       asel,s,loc,z,0                            
    *ELSE
    *ENDIF
  *ENDDO
*ENDDO
!
*DEL,NrN,,NOPR
*DEL,N_N,,NOPR
*DEL,NrE,,NOPR
*DEL,N_E,,NOPR
*DEL,ELE,,NOPR 
*DEL,x1,,NOPR
*DEL,x2,,NOPR
*DEL,x3,,NOPR
*DEL,x4,,NOPR
*DEL,y1,,NOPR
*DEL,y2,,NOPR
*DEL,y3,,NOPR
*DEL,y4,,NOPR
*DEL,n_BA,,NOPR
*DEL,d_LB_L1,,NOPR
*DEL,d_LB_L1_check,,NOPR
*DEL,d_LB_L2,,NOPR
*DEL,d_LB_L2_check,,NOPR
*DEL,d_LB_L3,,NOPR
*DEL,d_LB_L3_check,,NOPR
*DEL,d_LB_L4,,NOPR
*DEL,d_LB_L4_check,,NOPR
*DEL,ALA_BF,,NOPR
*DEL,d_sum,,NOPR