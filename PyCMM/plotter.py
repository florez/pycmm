import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.interpolate import griddata
from copy import copy

from PyCMM.postprocess import ResultReader
import PyCMM.datastructures as DS

# --------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------

class MVNPlotter(object):
    """
    Plots for HERO overpass

    args:   
    - data: ResultReader = data container to plot.
    - ndivx: int = gridpoints x-direction (default=25)
    - ndivy: int = gridpoints y-direction (default=14)
    """

    _geo_frame = np.array([[0, 0], [9700, 0], [9700, 5400], [0, 5400], [0, 0]])
    _xlim, _ylim = [0, 9700], [0, 5400]
    _half_line = np.array([[0, 5400/2], [9700, 5400/2]])
    _y_crit = 379 + 200


    def __init__(self, data: ResultReader, ndivx: int = 25, ndivy: int = 14) -> None:
        self.ndivx, self.ndivy = ndivx, ndivy
        self.data = data

    def plotFrame(self, ax):
        """
        Plots a rectangle around the plate area.
        """
        ax.plot(self._half_line.T[0], self._half_line.T[1], alpha=1, color='k', linestyle=(0, (7, 5)))
        [ax.hlines(i, *self._xlim, color='k') for i in self._ylim]
        [ax.vlines(i, *self._ylim, color='k') for i in self._xlim]
        ax.set_xlim(self._xlim); ax.set_ylim(self._ylim)
        return

    def createGridData(self, xi: list, yi: list, zi: list, factor=1, interpolation_method: str = 'linear') -> np.array:
        """
        Interpolate the data onto a regular grid in the X, Y plane.
        """
        X, Y = np.meshgrid(
            np.linspace(xi.min(), xi.max(), factor*self.ndivx),
            np.linspace(yi.min(), yi.max(), factor*self.ndivy)
        )
        Z = griddata((xi, yi), zi, (X, Y), method=interpolation_method)

        return X, Y, Z

    @staticmethod
    def _getUnitfromField(field): # Convenience function for titling, unnecessairy
        if field[0] in ('m', 'M'):
            return 'MNm/m'
        else:
            return 'MN/m'

    @staticmethod
    def getPrincipal(mx: float, my: float, mxy: float) -> np.array:
        """
        Creates 2d tensor and returns eigenvalues and eigenvectors.
        """
        tensor = np.array([
            [mx, mxy/2],
            [mxy/2, my]
        ])
        return np.linalg.eig(tensor)

    # -------------------------------------------------------------------------------------------    
    # PLOT METHODS
    # -------------------------------------------------------------------------------------------    

    def plotContour(self, 
        ax: plt.axes, 
        data: np.ndarray = None,
        field: str = None, 
        steps: int or list = 8, 
        plane: str = 'xy',
        **kwargs
    ) -> plt.axes:
        """
        Basic Contour plot.

        args:   
        - ax = A Matplotlib axes object.
        - data: OPTIONAL, array = data field to plot (e.g. mvn.mx)
        - field: OPTIONAL, str = Field to plot, e.g. 'mx', 'my' or 'mxy'. (If field is provided, data can be omitted)
        - steps: int or list = Nb of contours drawn. (default = 8).
        - plane: str = 'xy', 'xz' Use 'xz' to plot the walls.

        kwargs: 
        - Any other named parameters are passed to the matplotlib CONTOUR plot (check documentation for options).
        """
        assert not ((data is None) and (field is None)), 'Either data or a field must be provided!'
        if data is None:    data = getattr(self.data.mvn, field)
        if field is None:   field = 'xyy' # Arbitrary value 

        if plane=='xz': # wall plotting
            x, y = self.data.mvn.coords[:, 0], self.data.mvn.coords[:, 2] # simply use z-coordinate instead of y-coordinate
        else:
            x, y = self.data.mvn.coords[:, 0], self.data.mvn.coords[:, 1]
            self.plotFrame(ax)

        X, Y, Z = self.createGridData(x, y, data, factor=4)
        unit = self._getUnitfromField(field)

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(rf'${field[0]}_{{{field[1:]}}}$ [{unit}]')
        ax.set_xticks([]); ax.set_yticks([])

        # Contour plot
        CS = ax.contour(X, Y, Z, steps, colors='grey', **kwargs)
        ax.clabel(CS, inline=1, fontsize=8)

        # Show max/min values
        maxidx = np.unravel_index(Z.argmax(), Z.shape)
        ax.plot(X[maxidx], Y[maxidx], 'k+', markersize=0)
        ax.text(X[maxidx], Y[maxidx], rf'$\leftarrow${Z[maxidx]:.0f}', verticalalignment='center')

        minidx = np.unravel_index(Z.argmin(), Z.shape)
        ax.plot(X[minidx], Y[minidx], 'k+', markersize=0)
        ax.text(X[minidx], Y[minidx], rf'$\leftarrow${Z[minidx]:.0f}', verticalalignment='center')

        return ax


    def plotPrincipal(self, ax: plt.axes, scaling_length: float = 1.0, field: str = 'm', width=0.005, width_scaler=5, plane: str = 'xy', **kwargs) -> plt.axes:
        """
        Plot a Vector field of the principal moments.

        args:   
        - ax = A Matplotlib axes object.
        - scaling_length: float = Scaling parameter for velocity plot.
        - field: str = 'm' or 'n' (Moment or Membrane plot)
        - width: float = parameter to control the width of the lines (default=0.005).
        - width_scaler: float = parameter to control the maximal width variation of the lines (default=5).
        - plane: str = 'xy', 'xz' Use 'xz' to plot the walls.

        kwargs: 
        - Any other named parameters are passed to the matplotlib.pyplot QUIVER plot (check documentation for options).
        """
        data = self.data.mvn
        # Define keys regarding the desired field
        fx, fy, fxy = f'{field}x', f'{field}y', f'{field}xy'
        A = np.vstack((getattr(data, fx), getattr(data, fy), getattr(data, fxy))).T
        if plane=='xz': # wall plotting
            x, y = data.coords[:, 0], data.coords[:, 2]
        else:
            x, y = data.coords[:, 0], data.coords[:, 1]
            self.plotFrame(ax)

        wi = np.array([self.getPrincipal(*i)[0] for i in A])
        vi = np.array([self.getPrincipal(*i)[1] for i in A])
        v1, v2 = vi[:,:,0], vi[:,:,1]
        sigma1, sigma2 = wi[:, 0].reshape((-1, 1)) * v1, wi[:, 1].reshape((-1, 1)) * v2

        # Grid data for the 2 principal modes
        X, Y, Sig1 = self.createGridData(x, y, sigma1, interpolation_method='nearest')
        X, Y, Sig2 = self.createGridData(x, y, sigma2, interpolation_method='nearest')
        X, Y, Sig = self.createGridData(x, y, wi, interpolation_method='nearest')

        max_width, min_width = width_scaler * width, width

        for i, mode in enumerate([Sig1, Sig2]):
            for xi, yi, u, v, value_princ in zip(X.flatten(), Y.flatten(), mode[:,:,0].flatten(), mode[:,:,1].flatten(), Sig[:, :, i].flatten()):
                
                w = np.linalg.norm([u,v]) / np.abs(wi).max()  * (max_width - min_width) + min_width
    
                color = 'red' if value_princ>0 else 'blue'
                ax.quiver(
                    xi, yi, u, v, 
                    color = color,
                    headwidth=1, headlength=0, headaxislength=0, pivot='mid', width=w, minlength=0.1,
                    angles='xy', scale_units='xy', scale=scaling_length,
                    **kwargs
                )

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(rf'${field}_{{princ}}$')
        ax.set_xticks([]); ax.set_yticks([])
        return ax


    def plotElement(self, ax, el_nb, **kwargs):
        from matplotlib.patches import Rectangle
        data = self.data.shear
        xi, yi = data.coords.T
        x, y = data.coords[el_nb]
        el_size_x = np.diff(np.sort(np.unique(xi)))[0]
        el_size_y = np.diff(np.sort(np.unique(yi)))[0]
        ax.add_patch(Rectangle(xy=(x-el_size_x/2, y-el_size_y/2), width=el_size_y, height=el_size_x, **kwargs))
        return ax



    def plotShearflow(self, ax: plt.axes, intensity=5, color='grey', density=2, normalized_intensity=True, interpolation_method='linear', plot_frame=True, **kwargs) -> plt.axes:
        """
        Plot the shearflow with streamlines.

        args:   
        - intensity: number = thickness of lines
        - color: str = color
        - density: number = density of streamlines
        - normalized_intensity: bool = normalize intensity
        - interpolation_method: str = 'linear', 'cubic', 'nearest' (scipy interpolation griddata is used here)
        
        kwargs: 
        - Any other named parameters are passed to the matplotlib.pyplot STREAMPLOT plot (check documentation for options).
        """
        data = self.data.shear
        xi, yi = data.coords.T

        X, Y, VX = self.createGridData(xi, yi, data.v_xd, factor=4, interpolation_method=interpolation_method)
        X, Y, VY = self.createGridData(xi, yi, data.v_yd, factor=4, interpolation_method=interpolation_method)
        VTOT = np.sqrt(VX**2 + VY**2)

        # Manual startpoints to increase density without more lines :(
        ps=np.array([
            np.hstack((X[0], X[-1], X[1:-1,0], X[1:-1, -1])), 
            np.hstack((Y[0], Y[-1], Y[1:-1,0], Y[1:-1, -1]))
            ]).T

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(r'$v_{princ}$')
        ax.set_xticks([]); ax.set_yticks([])
        if plot_frame: self.plotFrame(ax)

        # Plot streamline
        if normalized_intensity:
            intensity = intensity*VTOT/VTOT.max()
        else:
            intensity = intensity*VTOT*1e-3
        ax.streamplot(
            X, Y, -VX, -VY, 
            color=color,
            linewidth=intensity,
            arrowsize=0,
            start_points=ps,
            density=density,
            **kwargs
        )

        return ax


    def plotShearCheck(self, ax: plt.axes, intensity=5, color_1='grey', color_2='red', density=2, annotate=True, normalized_intensity=True, interpolation_method='linear', linewidth=1, plot_frame=True, crit_marker_size=100, **kwargs) -> plt.axes:
        """
        Plot shearflow with Ausnutzung

        args:   
        - intensity: number = thickness of lines
        - color_1: str = color 1
        - color_2: str = color 2
        - density: number = density of streamlines
        - annotate: boot = Whether the critical point is marked
        - normalize_intensity: bool = normalize intensity
        - interpolation_method: str = 'linear', 'cubic', 'nearest' (scipy interpolation griddata is used here)
        
        kwargs: 
        - Any other named parameters are passed to the matplotlib.pyplot STREAMPLOT plot (check documentation for options).
        """
        from matplotlib.colors import ListedColormap
        ax.set_aspect('equal')
        ax.set_title(r'$v_{princ}$')
        ax.set_xticks([]); ax.set_yticks([])
        if plot_frame: self.plotFrame(ax)

        data = self.data.shear

        xi, yi = data.coords.T

        X, Y, VX = self.createGridData(xi, yi, data.v_xd, factor=4, interpolation_method=interpolation_method)
        X, Y, VY = self.createGridData(xi, yi, data.v_yd, factor=4, interpolation_method=interpolation_method)
        X, Y, Vaus = self.createGridData(xi, yi, data.v_aus, factor=4, interpolation_method=interpolation_method)
        VTOT = np.sqrt(VX**2 + VY**2)
        Mask = np.where(Vaus>=1, True, False)

        # Manual startpoints to increase density without more lines :(
        ps=np.array([
            np.hstack((X[0], X[-1], X[1:-1,0], X[1:-1, -1])), 
            np.hstack((Y[0], Y[-1], Y[1:-1,0], Y[1:-1, -1]))
            ]).T


        ax.set_title(r'$v_{princ}$ mit Ausnutzung')

        # Plot streamlines red
        if normalized_intensity:
            intensity_red = intensity*VTOT/VTOT.max()
        else:
            intensity_red = intensity*VTOT*1e-3
        color = np.empty_like(VX)
        color[:, :] = 1
        color[Mask==False] = 0
        cmap = ListedColormap([color_1]*20 + [color_2])

        ax.streamplot(
            X, Y, -VX, -VY, 
            color=color,
            cmap=cmap,
            linewidth=intensity_red,
            arrowsize=0,
            start_points=ps,
            density=density,
            **kwargs,
        )

        # Plot Nachweisschnitt
        bot_line, top_line = self._ylim[0] + self._y_crit, self._ylim[1] - self._y_crit
        ax.hlines(bot_line, xmin=self._xlim[0], xmax=self._xlim[1], color='red', linestyle=(0, (7, 5)), linewidth=linewidth)
        ax.hlines(top_line, xmin=self._xlim[0], xmax=self._xlim[1], color='red', linestyle=(0, (7, 5)), linewidth=linewidth)

        # Show Ausnutzungsziffer
        indices_relevant = np.where((yi<top_line) & (yi>bot_line))[0]
        yy, xx = yi[indices_relevant], xi[indices_relevant]
        ranks = np.argsort(data.v_aus[indices_relevant])

        if annotate:
            ax.text(
                xx[ranks[-1]], yy[ranks[-1]], 
                rf'$\leftarrow${data.v_aus[indices_relevant][ranks[-1]]:.2f}', verticalalignment='center'
            )
            ax.text(
                xx[ranks[-2]], yy[ranks[-2]], 
                rf'$\leftarrow${data.v_aus[indices_relevant][ranks[-2]]:.2f}', verticalalignment='center'
            )
        
        crit = np.where(data.v_aus[indices_relevant] >= 1)[0]
        ax.scatter(xx[crit], yy[crit], marker='x', s=crit_marker_size, color='k', zorder=4)

        return ax


    def _plotNachweisschnitt(self, ax, y_crit, linewidth=1):
        # Plot Nachweisschnitt
        bot_line, top_line = self._ylim[0] + y_crit, self._ylim[1] - y_crit
        ax.hlines(bot_line, xmin=self._xlim[0], xmax=self._xlim[1], color='red', linestyle=(0, (7, 5)), linewidth=linewidth)
        ax.hlines(top_line, xmin=self._xlim[0], xmax=self._xlim[1], color='red', linestyle=(0, (7, 5)), linewidth=linewidth)
        return
        

    def plotSteel(self, ax: plt.axes, field: str, scaling_length=1, color_yield='red', color_1='blue', width=0.005, yield_only=False, annotate=True, plane: str = 'xy', **kwargs):
        """
        Plot the steel stresses at cracks, with info if yielded or not.

        args:   
        - field: str = 'top_x', 'top_y', 'bot_x', 'bot_y'
        - scaling_length: number = parameter to control length of lines (default=1)
        - color_yield: str = color for yielding (default=red)
        - color_1: str = color for stress (default=blue)
        - width: float = width of lines (default=0.005)
        - yield_only: bool = if true, only the yielding is plotted (default=False)
        - annotate: bool = if true the critical point is annotated (default=True)
        - plane: str = 'xy', 'xz' Use 'xz' to plot walls.
        
        kwargs: 
        - Any other named parameters are passed to the matplotlib.pyplot QUIVER plot (check documentation for options).        
        """

        data = getattr(self.data.steel, field)
        
        sx, sy, sxy = data.sig_x, data.sig_y, data.sig_xy
        A = np.vstack((sx, sy, sxy)).T
        if plane=='xz':
            x, y = data.coords[:, 0], data.coords[:, 2]
        else:
            x, y = data.coords[:, 0], data.coords[:, 1]
            self.plotFrame(ax)

        wi = np.array([self.getPrincipal(*i)[0] for i in A])
        vi = np.array([self.getPrincipal(*i)[1] for i in A])
        v1, v2 = vi[:,:,0], vi[:,:,1]
        sigma1, sigma2 = wi[:, 0].reshape((-1, 1)) * v1, wi[:, 1].reshape((-1, 1)) * v2

        # Grid data for the 2 principal modes, Sig1 is a vector (dx, dy) of the principal stress
        X, Y, Sig1 = self.createGridData(x, y, sigma1, interpolation_method='nearest')
        X, Y, Sig2 = self.createGridData(x, y, sigma2, interpolation_method='nearest')
        Yield_1 = np.where(np.any(np.abs(Sig1) / self.data.parameters['fsy_P'] >= 1, axis=2), True, False)
        Yield_2 = np.where(np.any(np.abs(Sig2) / self.data.parameters['fsy_P'] >= 1, axis=2), True, False)

        for (mode, yield_bool) in zip([Sig1, Sig2], [Yield_1, Yield_2]):
            mode_cp = copy(mode)
            mode_cp_m_u, mode_cp_m_v = mode_cp[:, :, 0][yield_bool], mode_cp[:, :, 1][yield_bool]
            X_m, Y_m = X[yield_bool], Y[yield_bool]
            quiv = ax.quiver(
                X_m, Y_m, mode_cp_m_u, mode_cp_m_v,
                color=color_yield,
                headwidth=1, headlength=0, headaxislength=0, pivot='mid',
                width=2*width,
                minlength=0.1, alpha=.8,
                angles='xy', scale_units='xy', scale=scaling_length,
                **kwargs,
            )
            # pattern for yield bars
            if 'hatch' in kwargs:
                quiv.set_hatch(kwargs.get('hatch'))
        
        if not yield_only:
            for mode in [Sig1, Sig2]:
                for xi, yi, u, v in zip(X.flatten(), Y.flatten(), mode[:, :, 0].flatten(), mode[:, :, 1].flatten()):
                    w = max(
                        np.linalg.norm([u, v]) / self.data.parameters.fsu_P * width,
                        0.3 * width
                    )
                    ax.quiver(
                        xi, yi, u, v, 
                        color = color_1,
                        headwidth=1, headlength=0, headaxislength=0, pivot='mid', width=w, minlength=0.05,
                        angles='xy', scale_units='xy', scale=scaling_length,
                        **kwargs,
                    )
        


        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(fr'$\sigma_{{sr,{field}}} \ \mathrm{{[N/mm^2]}}$')
        ax.set_xticks([]); ax.set_yticks([])

        # Show max/min values
        if annotate:
            max_principal_stress = np.abs(wi).max(axis=1)
            maxidx = max_principal_stress.argmax()
            ratio = max_principal_stress[maxidx] / self.data.parameters['fsu_P']
            ax.plot(x[maxidx], y[maxidx], 'k+', markersize=0)
            ax.text(x[maxidx], y[maxidx], rf'$\leftarrow${max_principal_stress[maxidx]:.0f} ({ratio:.2f})', verticalalignment='center')

        return ax


    def plotConcrete(self, ax: plt.axes, field: str, scaling_length=0.05, width=0.001, width_scaler=5, plane: str = 'xy', **kwargs) -> plt.axes:
        """
        Plot the concrete compression in the plate.

        args:   
        - field: str = 'top', 'mid', 'bot'
        - scaling_length: float = parameter to control length of lines (default=0.05)
        - width: float = parameter to control width of lines (default=0.001)
        - width_scaler: float = largest width is this times the smallest width
        - plane: str = 'xy', 'xz' Use 'xz' to plot walls.
        
        kwargs: 
        - Any other named parameters are passed to the matplotlib.pyplot QUIVER plot (check documentation for options).
        """
        
        data: DS.Concrete.ConcLayer = getattr(self.data.conc, field)
        
        x = data.coords.T[0]
        if plane=='xz': # For WALL plotting
            y = data.coords.T[2] 
        else:
            y = data.coords.T[1]
            self.plotFrame(ax)

        c1, c3, eps = data.sigma_c1, data.sigma_c3, data.eps
        
        wi = np.array([self.getPrincipal(*i)[0] for i in eps])
        vi = np.array([self.getPrincipal(*i)[1] for i in eps])
        
        v1, v2 = vi[:,:,0], vi[:,:,1]
        sigma1, sigma2 = c1.reshape((-1, 1)) * v1, c3.reshape((-1, 1)) * v2

        # Ausnutzung
        fcc = self.data.parameters.fcd_P
        eps_1 = wi[:, 0]
        eps_1[eps_1 <= 0] = 0
        fcc_vec = -(fcc**(2/3) / (0.4 + 30*eps_1) )
        ratio = c3 / fcc_vec

        X, Y, Sig1 = self.createGridData(x, y, sigma1, interpolation_method='nearest')
        X, Y, Sig2 = self.createGridData(x, y, sigma2, interpolation_method='nearest')
        X, Y, C3 = self.createGridData(x, y, c3, interpolation_method='nearest')
        X, Y, C1 = self.createGridData(x, y, c1, interpolation_method='nearest')

        max_width, min_width = width_scaler*width, width

        # This is an extra loop -> Slow, but with thickness intensity
        for mode, value in zip([Sig1, Sig2], [C1, C3]):
            for (xi, yi, u, v, val) in zip(X.flatten(), Y.flatten(), mode[:, :, 0].flatten(), mode[:, :, 1].flatten(), value.flatten()):
                ax.quiver(
                    xi, yi, u, v, 
                    color = 'forestgreen' if val<0 else 'red',
                    headwidth=1, headlength=0, headaxislength=0, pivot='mid',
                    width= np.abs(val/c3.min()) * (max_width - min_width) + min_width, 
                    minlength=0.1,
                    angles='xy', scale_units='xy', scale=scaling_length,
                    **kwargs
                )
        

        # Axes setup
        ax.set_aspect('equal')
        ax.set_title(rf'$\sigma_{{c3,{field}}} \ \mathrm{{[N/mm^2]}}$')
        ax.set_xticks([]); ax.set_yticks([])

        # Show minimum
        minidx = c3.argmin()
        ax.plot(x[minidx], y[minidx], 'k+', markersize=0)
        ax.text(x[minidx], y[minidx], rf'$\leftarrow${c3[minidx]:.0f}', verticalalignment='center')

        # Show max ratio
        ratioidx = ratio.argmax()
        ax.plot(x[ratioidx], y[ratioidx], 'kx', markersize=5)
        ax.text(x[ratioidx], y[ratioidx], rf'{ratio[ratioidx]:.2f}$\rightarrow$', va='center', ha='right')

        return ax


        