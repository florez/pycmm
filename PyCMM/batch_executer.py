import os, subprocess, json
import multiprocessing

from .preprocess import loadConfig
from .utilities import makePlots

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

class ANSYSRun:
    """
    Execution of ANSYS files.

    Arguments
    ---------
    - name: str = name of simulation (used to name output data)
    - work_dir: str = working directory of simulation / executable
    - input_file: str = Input file name (default=00_Mainfile.inp)
    - output_file: str = Output file name (default=<name>_output.out)
    """
    def __init__(self) -> None:
        pass

    def initialize(self, 
        name: str, 
        work_dir: str, 
        input_file: str = '00_Mainfile.inp', 
        output_file: str = None,
    ):

        self.name = name
        self.work_dir = work_dir
        self.input_file = os.path.join(work_dir, input_file)
        output_file = f'{name}_output.out' if output_file is None else output_file
        self.output_file = os.path.join(work_dir, output_file)
        self.loadSetup()

        return self

    def loadSetup(self):
        try:
            with open(os.path.join(self.work_dir, 'setup.inp'), 'r') as f:
                self.setup = json.loads(f.read())
        except FileNotFoundError:
            self.setup = loadConfig()
    
    def launchANSYS(self):

        ansys_path =    self.setup['ansys_path']
        lic_str =       self.setup['lic_str']
        cpus =          self.setup['cpus']

        launch_string = f""""{ansys_path}" -b -p {lic_str} -np {cpus} -dir "{self.work_dir}" -j {self.name} -s read -l en-us -i "{self.input_file}" -o "{self.output_file}" -m 2000 -db 1000 """

        subprocess.run(launch_string)

        return self

    
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------



# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------




def SingleCall(args: tuple):
    """
    Run the specified simulation. args given in a tuple (needed for multiprocessing.Pool).

    Arguments
    ---------
    - simname: str
    - folder: str
    - delete: bool = True
    """
    import fnmatch
    
    name, folder, delete = args
    path_to =   os.path.join(folder, name)
    print(f'Starting sim {name}')

    Caller =    ANSYSRun().initialize(name, path_to)
    try:    Caller.launchANSYS()
    except KeyboardInterrupt: return

    makePlots(os.path.join(folder, name)) # Create all plots and save them in PDF files

    if delete:
        # Delete large files again, comment this if you need it
        for root, subdir, files in os.walk(os.path.join(folder, name)):
            for file in fnmatch.filter(files, f'{name}*.r*'):
                os.remove(os.path.join(root, file))
            for file in fnmatch.filter(files, f'{name}*.esav*'):
                os.remove(os.path.join(root, file))
            for file in fnmatch.filter(files, 'Solu_.db'):
                os.remove(os.path.join(root, file))

    return

# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

def runAll(folder=os.path.abspath('sim_inputs'), parallel=3, tasks=None, delete=True):
    """
    Run a series of simulations in parallel.

    Arguments
    ---------
    - folder: str = directory where simulations are.
    - paralell: int = number of paralell simulations.
    - tasks: list = subset of simulations in folder.
    - delete: bool = Whether most files are deleted after finishing.
    """

    if tasks is None:
        tasks = ((sim, folder, delete) for sim in os.listdir(folder))
    else:
        tasks = [(i, folder, delete) for i in tasks]

    pool = multiprocessing.Pool(processes=parallel)
    r = pool.map(func=SingleCall, iterable=tasks)
    r.wait()
    pool.close()
    
    return


# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------