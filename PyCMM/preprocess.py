"""
Author: florez@ethz.ch
Dated: 07.06.22

Preprocessing routines
"""

import importlib.resources
import os, shutil, json


from . import config, templates



def writeParameterFile(parameters: dict, directory: str, filename: str = 'Parameterfile.inp'):
    """
    Take dictionary with parameters and write it to file for ANSYS to read.
    """
    filename = os.path.join(directory, filename)
    header = importlib.resources.read_text(templates, 'header_parameterfile.txt')

    with open(filename, 'a') as f:
        f.write(header)
        for key, value in zip(parameters.keys(), parameters.values()):
            f.write(f'{key} = {value:.4f}\n')

    return


def writeSetup(setup: dict, directory: str):
    """
    Take dictionary and dump it as json file.
    """
    with open(os.path.join(directory, 'setup.inp'), 'w') as f:
        f.write(json.dumps(setup))
    return


def loadConfig():
    """
    Read the config file to get standard ANSYS configuration.
    """
    setup = importlib.resources.read_text(config, 'ansys_setup.txt')
    lines = setup.splitlines()
    lines = [l for l in lines if '=' in l]
    lines = [l.split('#', maxsplit=1)[0] for l in lines]
    lines = [l.split('=', maxsplit=1) for l in lines]

    setup_dict = {}
    for key, val in lines:
        key, val = key.strip(), val.strip()
        val = val.replace('\'', '')
        try:
            setup_dict[key] = int(val)
        except ValueError:
            setup_dict[key] = str(val)

    return setup_dict


def createWorkDirectory(work_dir, model_directory):
    """
    Create the simulation directory.

    args:   
    - work_dir: str = simulation directory.
    - model_directory: str = directory of ANSYS model files.
    """
    if os.path.exists(work_dir):
        ans = input('This directory does already exist, do you want to overwrite it? (y/n)')
        if ans in ('yes', 'y'):
            shutil.rmtree(work_dir)
        else:
            raise RuntimeError('Abort. Directory exists!')
    os.mkdir(work_dir)

    for file in os.listdir(model_directory):
        shutil.copy(os.path.join(model_directory, file), os.path.join(work_dir, file))
    
    return

# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------


# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

def createSim(
        parameters: dict, 
        directory: str = 'sim_inputs', 
        setup: dict = None, 
        model_directory: str = None,
        base_name: str = 'sim',
        full_name: str = None,
    ):
    """
    Crate a simulation by copying model files into specified directory.

    args:   
    - parameters: dict = parameters for simulation
    - directory: str = directory to paste the simulation
    - setup: OPTIONAL, dict = alternative ANSYS configuration (standard is loaded from PyCMM/config)
    - model_directory: str = directory of ANSYS model files
    - base_name: OPTIONAL, str = basename for simulations, the numbering is done automatically.
    - full_name: OPTIONAL, str = no numbering, use this name.
    """


    if model_directory is None: 
        model_directory=os.path.join(loadConfig()['work_directory'], 'APDLmodelTraglast')

    config_dict = loadConfig()
    sim_counter = os.path.join(config_dict['work_directory'], f'counter_{base_name}.txt')
    if setup is None: setup = config_dict
        
    # get current sim number --------------------------
    try:
        with open(sim_counter, 'r') as f:
            simnumber = int(f.read())
    except FileNotFoundError: 
        simnumber = 0

    simname = f'{base_name}_{simnumber:03d}'
    simnumber += 1
    with open(sim_counter, 'w') as f:
        f.write(str(simnumber))
    # -------------------------------------------------

    simname = full_name if full_name else simname
    sim_dir = os.path.join(directory, simname)
    
    createWorkDirectory(sim_dir, model_directory)
    writeParameterFile(parameters, directory=sim_dir, )
    writeSetup(setup, directory=sim_dir)

    return


# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------
