from pathlib import Path
import matplotlib.pyplot as plt
import os
import importlib.resources


def setTheme():
    import seaborn as sbn
    sbn.set_theme(
        style="ticks", rc={
            "axes.spines.right": True, 
            "axes.spines.top": True, 
            'axes.grid':True, 
            'text.usetex':True, 
            'font.family':'serif'
        }
    )
    return None

def setPlotThemeReport():
    import seaborn as sbn
    sbn.set_theme(
        style="ticks", rc={
            "axes.spines.right": True, 
            "axes.spines.top": True, 
            'axes.grid':True, 
            'text.usetex':True, 
            'font.family':'serif'
        }
    )
    plt.rcParams.update({
        'font.size': 8, 
        'axes.linewidth': .5, 
        'grid.linewidth': .5, 
        'grid.linestyle': (5,8), 
        'lines.linewidth': .5, 
        'xtick.major.width': .5, 'xtick.minor.width': .3, 'xtick.minor.size': 2,
        'ytick.major.width': .5, 'ytick.minor.width': .3, 'ytick.minor.size': 2,
        'axes.labelsize': 8, 
        'lines.markersize': 3, 
        'lines.markeredgewidth': .5, 
        'xtick.labelsize': 8, 
        'ytick.labelsize': 8, 
        'axes.titlesize': 8, 
        'xtick.major.size': 3, 
        'ytick.major.size': 3,
        'xtick.direction': 'in',
        'ytick.direction': 'in',
        'figure.dpi': 150,
    })
    plt.rcParams['legend.fontsize'] = 8
    plt.rcParams['legend.fancybox'] = False
    plt.rcParams['legend.borderpad'] = 0.2
    plt.rcParams['legend.edgecolor'] = '0'
    plt.rcParams['legend.framealpha'] = .2
    plt.rcParams['patch.linewidth'] = .3
    plt.rcParams['legend.handlelength'] = 1
    plt.rcParams['legend.shadow'] = False
    plt.rcParams['legend.labelspacing'] = 0.3
    return None


def makePlots(path_to_folder: str, output_path: str = None):
    """
    Create all plots for a simulation and save them as pdf files.

    args:   
    - path_to_folder: str = path to simulation
    - output_path: str = path to folder where figures are saved (default=<path_to_folder>/figures)
    """

    from PyCMM import ResultReader, MVNPlotter
    setTheme()

    if output_path is None:
        output_path = os.path.join(path_to_folder, 'figures')

    # ------------------------------------------------------
    # PLOT ALL
    # ------------------------------------------------------


    # Load results
    Reader = ResultReader(path_to_folder)
    Reader.readMVN()
    Reader.readSteel()
    Reader.readConcrete()
    Reader.readShear()

    # Init plotter
    plotter = MVNPlotter(Reader, ndivx = 25, ndivy = 14)

    # Set folder and names for figs
    Path(output_path).mkdir(parents=True, exist_ok=True)
    base_name = os.path.normpath(path_to_folder).split(os.sep)[-1]

    # Concrete plots
    # -------------------------------------------
    fig, axes = plt.subplots(1, 2, figsize=(16, 10))

    plotter.plotConcrete(axes[1], 'top', scaling_length=0.05, width=0.0002, width_scaler=10)
    plotter.plotConcrete(axes[0], 'bot', scaling_length=0.05, width=0.0002, width_scaler=10)

    plt.savefig(f'{output_path}/{base_name}_sig_c3.pdf', facecolor='w')

    # Steel plots
    # -------------------------------------------
    fig, axes = plt.subplots(2, 2, figsize=(16,10))

    plotter.plotSteel(axes[0, 0], field='bot_y', scaling_length=0.7, width=0.005)
    plotter.plotSteel(axes[0, 1], field='bot_x', scaling_length=0.7, width=0.005)
    plotter.plotSteel(axes[1, 0], field='top_x', scaling_length=0.7, width=0.005)
    plotter.plotSteel(axes[1, 1], field='top_y', scaling_length=0.7, width=0.005)

    plt.savefig(f'{output_path}/{base_name}_sig_sr.pdf', facecolor='w')

    # Membrane plots
    # -------------------------------------------

    fig, axes = plt.subplots(2, 2, figsize=(16, 10))

    plotter.plotContour(axes[0, 0], field='nx')
    plotter.plotContour(axes[0, 1], field='ny')
    plotter.plotContour(axes[1, 0], field='nxy')
    plotter.plotPrincipal(axes[1, 1], scaling_length=1.5, width=0.0002, width_scaler=20, field='n')

    plt.savefig(f'{output_path}/{base_name}_n.pdf', facecolor='w')

    # Shear plots
    # -------------------------------------------

    fig, axes = plt.subplots(2, 2, figsize=(16, 10))

    plotter.plotContour(axes[0, 0], field='vx', data=Reader.shear.v_xd)
    plotter.plotContour(axes[0, 1], field='vy', data=Reader.shear.v_yd)
    plotter.plotContour(axes[1, 0], field='v0', data=Reader.shear.v_0d)
    plotter.plotShearflow(axes[1, 1], intensity=5)

    plt.savefig(f'{output_path}/{base_name}_v.pdf', facecolor='w')

    # Shear check plot
    # -------------------------------------------
    fig = plt.figure(figsize=(16, 10))
    ax = fig.add_subplot(111)

    plotter.plotShearCheck(ax, intensity=6.5, density=15, color_1='grey', color_2='red')
    
    plt.savefig(f'{output_path}/{base_name}_v_check.pdf', facecolor='w')

    # Moment plots
    # -------------------------------------------

    fig, axes = plt.subplots(2, 2, figsize=(16, 10))

    plotter.plotContour(axes[0, 0], field='mx')
    plotter.plotContour(axes[0, 1], field='my')
    plotter.plotContour(axes[1, 0], field='mxy')
    plotter.plotPrincipal(axes[1,1], scaling_length=1.5, width=0.0002, width_scaler=20, field='m')

    plt.savefig(f'{output_path}/{base_name}_m.pdf', facecolor='w')


    return None

# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

def loadSteps(file: str = 'steps.txt') -> dict:
    with open(file, 'r') as f:
        all_lines = f.read()
    lines = all_lines.splitlines()
    lines = [
        [float(i) for i in line.split()] 
        for line in lines if ('SET' not in line) & (len(line)>0)
    ]
    steps = [
        {'step':s[2], 'substep':s[3], 'set_nb': s[0], 'time_n': s[1]} 
        for s in lines
    ]
    return steps


def saveStepData(directory, name) -> str:
    """
    Extract all steps and substeps of a simulation.

    args:   
    - directory: str
    - name: str

    return: 
    - None
    """
    from .batch_executer import ANSYSRun
    
    # To save the steps of the simulation into .txt file
    executable = """
    /POST1

    RESUME,Solu_,db

    /OUTPUT,steps,txt
    SET,LIST
    /OUTPUT
    """

    with open('tmp_.inp', 'w') as f:
        f.write(executable)

    ANSYSRun().initialize(name, os.path.join(directory, name), 'tmp_.inp').launchANSYS()
    
    return 'Process finished!'

def saveAllSteps(directory, name):
    """
    Use the database to extract not only the results for the last step but for all steps
    and substeps.

    args:   
    - directory: str
    - name: str
    
    return: 
    - None
    """
    import shutil
    from .batch_executer import ANSYSRun
    from . import templates
    
    path_template = importlib.resources.path(templates, 'save_steps.inp')
    shutil.copy(path_template, os.path.join(directory, name))

    path = os.path.join(directory, name)
    steps = loadSteps(os.path.join(path, 'steps.txt'))
    
    def writeParameterFile(file: str, parameters: dict):
        with open(file, 'w') as f:
            for name, val in zip(parameters.keys(), parameters.values()):
                try: f.write(f'{name} = {val:.4f}\n')
                except ValueError: f.write(f"{name} = '{val}'\n")


    for i, step in enumerate(steps): 
        print(f'Writing step {i} / {len(steps)}...')
        writeParameterFile(os.path.join(path, 'save_steps_par.inp'), parameters= {'iteration': f's{i:02d}'} | step)
        ANSYSRun().initialize(name, os.path.join(directory, name), 'save_steps.inp').launchANSYS()

    # At some files, the extension of the file name is cut off, this is a fix.
    for root, dirs, files in os.walk(os.path.join(directory, name)):
        for file in files:
            filetmp = file.replace('.tx', '.txt')
            filenew = filetmp.replace('.txtt', '.txt')
            shutil.move(os.path.join(root, file), os.path.join(root, filenew))

    return 'Process finished!'