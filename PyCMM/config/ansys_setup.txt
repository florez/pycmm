## Ansys setup file
## For directories on Windows: Use double backslash (cause of python stuff)

work_directory = 'C:\\Users\\lorez\\dev\\2022_MT\\PyCMM'

ansys_path = 'C:\\Program Files\\ANSYS Inc\\v221\\ansys\\bin\\winx64\\ANSYS221.exe' # path to ansys executable
lic_str = 'aa_t_a' # License
cpus = 2
