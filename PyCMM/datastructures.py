"""
Author: florez@ethz.ch
Date: 07.06.22

"""



from typing_extensions import Self
import numpy as np
from dataclasses import dataclass, field
from scipy.optimize import root
from scipy.interpolate import interp2d, griddata





@dataclass
class Steel:
    """Structure to store steel data"""
    @dataclass
    class SteelLayer:
        coords: np.ndarray = None
        sig_x:  np.ndarray = None
        sig_y:  np.ndarray = None
        sig_xy: np.ndarray = None
        check:  np.ndarray = None

        def __post_init__(self):
            """If field is none change to zeros"""
            for field in [self.sig_x, self.sig_y, self.sig_xy]:
                if field is not None: shape=field.shape
            assert isinstance(shape, tuple), 'One field must be given!'
            for field in ['sig_x', 'sig_y', 'sig_xy']:
                if getattr(self, field) is None: setattr(self, field, np.zeros(shape))

        @property
        def stress(self): # return sig which is not zeros
            if np.any(self.sig_x!=0):
                return self.sig_x
            return self.sig_y
                

    bot_x: SteelLayer
    bot_y: SteelLayer
    top_x: SteelLayer
    top_y: SteelLayer
    coords_quads: np.ndarray = None

    def inCentroid(self, field, el_nb):
        f = griddata((self.top_y.coords[:, 0], self.top_y.coords[:, 1]), field, (self.coords_quads[el_nb, 0], self.coords_quads[el_nb, 1]), method='linear')
        return f

    

    


@dataclass
class Concrete:
    """Structure to store concrete data"""
    @dataclass
    class ConcLayer:
        coords:   np.ndarray = None
        eps:      np.ndarray = None
        theta_r:  np.ndarray = None
        sigma_c1: np.ndarray = None
        sigma_c3: np.ndarray = None
        eps_1:    np.ndarray = None
        eps_3:    np.ndarray = None
        fc_eps:   np.ndarray = None
        model_used: np.ndarray = None
    
    top: ConcLayer = None
    mid: ConcLayer = None
    bot: ConcLayer = None
    coords_quads: np.ndarray = None

    def inCentroid(self, field, el_nb):
        f = griddata((self.mid.coords[:, 0], self.mid.coords[:, 1]), field, (self.coords_quads[el_nb, 0], self.coords_quads[el_nb, 1]), method='linear')
        return f


@dataclass
class Shear:
    """Structure to store shear data"""
    tau_cd: float
    coords: np.ndarray = None
    v_rd:   np.ndarray = None
    v_0d:   np.ndarray = None
    v_xd:   np.ndarray = None
    v_yd:   np.ndarray = None
    d_v:    np.ndarray = None
    eps_v06d: np.ndarray = None

    _dv = 340

    def __post_init__(self):
        self.v_rd[self.v_rd==-1] = self.tau_cd * self._dv

    @property
    def theta_0(self):
        return np.arctan(self.v_yd / self.v_xd)

    @property
    def v_aus(self):
        return self.v_0d / self.v_rd

    def eps_v06d_el(self, el_nb):

        def func(eps, vrd, dv=340, D_max=32):
            kg = 48 / (16 + D_max)
            kd = 1 / (1 + 2.5 * eps*dv*kg)
            vrd_iter = kd * self.tau_cd * dv
            return vrd_iter - vrd

        res = root(func, 1e-2, self.v_rd[el_nb]).x[0]
        if res<0.005: return res
        else: return None

@dataclass
class CritElement:
    """Critical element"""
    El_nb:      int     = field(init=False)
    location:   list    = field(init=False)
    v_0d:       float   = field(init=False)
    v_rd:       float   = field(init=False)
    v_aus:      float   = field(init=False)
    theta_0:    float   = field(init=False)
    eps_v06d:   float   = field(init=False)
    
    _xlim = (0, 9700)   # HARD-CODED
    _ylim = (0, 5400)   # HARD-CODED
    crit_distance = 579 # HARD-CODED

    def autoFind(self, shear_data: Shear) -> Self:
        bot_line = self._ylim[0] + self.crit_distance
        top_line = self._ylim[1] - self.crit_distance
        xi, yi = shear_data.coords.T
        mask = np.where((yi<top_line) & (yi>bot_line), True, False)
        v_aus_ma = np.ma.array(shear_data.v_aus, mask=~mask)

        # Set attributes
        self.El_nb = v_aus_ma.argmax()
        self.eps_v06d = shear_data.eps_v06d_el(self.El_nb)
        self.v_0d = shear_data.v_0d[self.El_nb]
        self.v_rd = shear_data.v_rd[self.El_nb]
        self.v_aus = self.v_0d / self.v_rd
        self.theta_0 = shear_data.theta_0[self.El_nb]
        self.location = [xi[self.El_nb], yi[self.El_nb]]

        return self


@dataclass
class MVN:
    """Structure to store cross sectional internal forces"""
    coords: np.ndarray = None
    mx:     np.ndarray = None
    my:     np.ndarray = None
    mxy:    np.ndarray = None
    nx:     np.ndarray = None
    ny:     np.ndarray = None
    nxy:    np.ndarray = None


    # calculate principal moment and membrane force


@dataclass
class DeforRot:
    """Structure to store deformation, rotation data"""
    node_nb: int
    ux:     float
    uy:     float
    uz:     float
    rotx:   float
    roty:   float
    rotz:   float