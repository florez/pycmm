"""
Author: florez@ethz.ch
Date: 13.03.22
Updated: 07.06.22

"""

from typing_extensions import Self
import numpy as np
import os
import pandas as pd
from scipy.interpolate import griddata
from munch import Munch
from dataclasses import fields
from typing import List

import PyCMM.datastructures as DS
from PyCMM import utilities

class ResultReader(object):
    """
    Read ANSYS results.
    The data is read from the .txt files created in ANSYS. Therefore, if the writing is changed, the loading here must be too.
    One could automate this by naming the columns in the .txt files. So far, the knowledge is passed by the location in the files.
    The datastructure is written in datastructures.py. Different fields for Concrete, steel, etc.

    args:   
    - directory: str = path to simulation
    """

    _delimiter = 'Blank'
    _possible_plate_names = ['D0Platte', 'D1Platte', 'DTPlatte']
    _parameter_file = 'Parameterfile.inp'
    # _parameter_file = '12.2_Resistance_Parameter.inp'
    _figurepath = 'figures'
    

    def __init__(self, directory) -> None:
        self.directory = directory
        self.plate_name = self.getValidPlateName()
        self.readParameterFile()
        self.name = os.path.basename(directory)

    def setPlateName(self, name):
        self.plate_name = name
        return self


    def getValidPlateName(self) -> str:
        """
        Depending on k_riss, the files are named differently.
        """
        for name in self._possible_plate_names:
            if os.path.isfile(os.path.join(self.directory, f'{name}_mvn.txt')):
                return name


    def readParameterFile(self) -> None:
        parameters = {}
        with open(os.path.join(self.directory, self._parameter_file)) as file:
            for line in file:
                if '=' in line:
                    key, val = line.strip().split('=')
                    key = key.strip()
                    parameters[key] = float(val)

        self.parameters = Munch(**parameters)

    
    # -----------------------------------------------------------------------------------------
    # Methods to read different files
    # -----------------------------------------------------------------------------------------

    def readDeforRot(self) -> DS.DeforRot:

        file = os.path.join(self.directory, f'{self.plate_name}_defor_rot.txt')
        arr = np.loadtxt(file)

        self.defor_rot = DS.DeforRot(
            node_nb=    arr[:, 0],
            ux=         arr[:, 1],
            uy=         arr[:, 2],
            uz=         arr[:, 3],
            rotx=       arr[:, 4],
            roty=       arr[:, 5],
            rotz=       arr[:, 6],
        )

        return self.defor_rot

    def readMVN(self) -> DS.MVN:

        file = os.path.join(self.directory, f'{self.plate_name}_mvn.txt')
        arr = np.loadtxt(file)

        self.mvn = DS.MVN(
            coords= arr[:, 10:],
            mx=     arr[:, 1].flatten() * 1e-3,
            my=     arr[:, 2].flatten() * 1e-3,
            mxy=    arr[:, 3].flatten() * 1e-3,
            nx=     arr[:, 7].flatten(),
            ny=     arr[:, 8].flatten(),
            nxy=    arr[:, 9].flatten(),
            # vx=     arr[:, 4].flatten(),
            # vy=     arr[:, 5].flatten(),
        )
        
        return self.mvn

    def readShear(self) -> DS.Shear:

        fileVNW = os.path.join(self.directory, f'{self.plate_name}_vNW.txt')
        arr = np.loadtxt(fileVNW)

        self.shear = DS.Shear(
            tau_cd= self.parameters['tau_cd'],
            coords= arr[:, [10, 11]],
            v_rd=   arr[:, 1],
            v_0d=   arr[:, 2],
            v_xd=   arr[:, 3],
            v_yd=   arr[:, 4],
            d_v=    arr[:, 5],
            eps_v06d= arr[:, 7],
        )

        self.critElement = DS.CritElement().autoFind(self.shear)
        
        return self.shear

    def readSteel(self) -> dict:
        """Read steel data"""

        @staticmethod
        def loadCoordinates(field):
            """field: bot_x, bot_y, top_x or top_y"""
            filename = f'{self.plate_name}_coor_layer_{field}.txt'
            return np.loadtxt(os.path.join(self.directory, filename))
        
        # File with steel stresses at cracks
        filename = os.path.join(self.directory, f'{self.plate_name}_sig_sr.txt')
        sig_sr = np.loadtxt(filename)

        # the aux file was accidentally with 10 columns instead of 12
        # for that reason, I had to load it differently, but it also should load if correct
        try:
            aux = np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_auxiliary.txt'))
        except ValueError:
            with open(os.path.join(self.directory, f'{self.plate_name}_auxiliary.txt'), 'r') as f:
                aux = list(map(str.split, f))
                aux = np.array([np.float_(i) for j in aux for i in j]).reshape((-1, 12))

        self.steel = DS.Steel(
            bot_x= DS.Steel.SteelLayer(
                coords= loadCoordinates('bot_x'),
                sig_x=  np.sum(sig_sr[:, 0:2], axis=1),
                check=  np.sum(aux[:, 1:3], axis=1),
            ),
            bot_y= DS.Steel.SteelLayer(
                coords= loadCoordinates('bot_y'),
                sig_y=  np.sum(sig_sr[:, 2:4], axis=1),
                check=  np.sum(aux[:, 4:6], axis=1),
            ),
            top_x= DS.Steel.SteelLayer(
                coords= loadCoordinates('top_x'),
                sig_x=  np.sum(sig_sr[:, 6:8], axis=1),
                check=  np.sum(aux[:, 7:9], axis=1),
            ),
            top_y= DS.Steel.SteelLayer(
                coords= loadCoordinates('top_y'),
                sig_y=  np.sum(sig_sr[:, 4:6], axis=1),
                check=  np.sum(aux[:, 10:12], axis=1),
            ),
            # coords_quads=self.mvn.coords,
        )

        return self.steel

    def readConcrete(self) -> DS.Concrete:

        data = {}
        arr_eps = np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_eps_princ.txt'))
        arr_sig_c = np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_sig_c.txt'))

        self.conc = DS.Concrete(
            top=    DS.Concrete.ConcLayer(
                coords= np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_coor_intp_toplayer.txt'))[:, :3],
                eps=    arr_eps[:, 0:3],
                theta_r=  arr_eps[:, 3],
                sigma_c1= arr_sig_c[:, 1],
                sigma_c3= arr_sig_c[:, 2],
                eps_1=    arr_eps[:, 4],
                eps_3=    arr_eps[:, 5],
                fc_eps=   arr_sig_c[:, 3],
                model_used= arr_sig_c[:, 0],
            ),
            mid=    DS.Concrete.ConcLayer(
                coords= np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_coor_intp_botlayer.txt'))[:, :3],
                eps=    arr_eps[:, 6:9],
                theta_r=  arr_eps[:, 9],
                sigma_c1= arr_sig_c[:, 5],
                sigma_c3= arr_sig_c[:, 6],
                eps_1=    arr_eps[:, 10],
                eps_3=    arr_eps[:, 11],
                fc_eps=   arr_sig_c[:, 7],
                model_used= arr_sig_c[:, 4],
            ),
            bot=    DS.Concrete.ConcLayer(
                coords= np.loadtxt(os.path.join(self.directory, f'{self.plate_name}_coor_intp_botlayer.txt'))[:, :3],
                eps=    arr_eps[:, 12:15],
                theta_r=  arr_eps[:, 15],
                sigma_c1= arr_sig_c[:, 9],
                sigma_c3= arr_sig_c[:, 10],
                eps_1=    arr_eps[:, 16],
                eps_3=    arr_eps[:, 17],
                fc_eps=   arr_sig_c[:, 11],
                model_used= arr_sig_c[:, 8],
            ),
            # coords_quads=self.mvn.coords,
        )


        return self.conc



    def readAll(self) -> Self:
        """Convenience function to read everything"""
        self.readMVN()
        self.readConcrete()
        self.readShear()
        self.readSteel()
        try:
            self.getFailure()
        except FileNotFoundError:
            print('Failure not detected, continue without it.')
        return self

    # -----------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------

    def inCentroid(self, data, element_numbers=None, method='linear'):
        """
        Get field in centroid of element.
        Steel & concrete data is in quad points (4 for SHELL182)

        args:   
        - data: array = data in quad points
        - element_numbers: list = element number for which data is needed. 'None' to return all elements.
        - method: str = interpolation method (scipy.interpolate.griddata)
        """
        if element_numbers is None:
            f = griddata(
                (self.steel.top_y.coords[:, 0], self.steel.top_y.coords[:, 1]), 
                data, (self.mvn.coords[:, 0], self.mvn.coords[:, 1]), 
                method=method,
            )
        else:
            f = griddata(
                (self.steel.top_y.coords[:, 0], self.steel.top_y.coords[:, 1]), 
                data, (self.mvn.coords[element_numbers, 0], self.mvn.coords[element_numbers, 1]), 
                method=method,
            )
        return f

    def getNeighbour(self, data, element_numbers: List[int], offset: float = -400):
        """
        Get field in neighbouring element. Specified by offset in mm.

        args:   
        - data: array = data
        - element_numbers: list = element number for which data is needed. 'None' to return all elements.
        - offset: float = offset in y-direction; Used to define the 'neighbour'.
        """
        return griddata((self.mvn.coords[:, 0], self.mvn.coords[:, 1]), data, (self.mvn.coords[element_numbers,0], self.mvn.coords[element_numbers,1] + offset))




    @staticmethod
    def getPrincipal(mx: float, my: float, mxy: float) -> np.array:
        """
        Creates 2d tensor and returns eigenvalues and eigenvectors.
        """
        tensor = np.array([
            [mx, mxy/2],
            [mxy/2, my]
        ])
        return np.linalg.eig(tensor)

    def getPrincMembrane(self):
        nx, ny, nxy = [self.mvn.__dict__[field] for field in ['nx', 'ny', 'nxy']]
        n3 = np.array([self.getPrincipal(nx, ny, nxy)[0].min() for nx, ny, nxy in zip(nx, ny, nxy)])
        return n3

    # -----------------------------------------------------------------------
    # result file
    # -----------------------------------------------------------------------
    def getFailure(self):
        """
        Read the failure information (failure_infos.txt).
        """
        
        file = os.path.join(self.directory, 'failure_infos.txt')
        
        with open(file, 'r') as f:
            lines = [line for line in f]

        def getTime(lines):
            for line in lines:
                if 'TIME STEP AT FAILURE' in line:
                    return float(line.split()[-1])
            return ValueError

        def getFailureMode(lines):
            for line in lines[5:]:
                splitted = line.split() # first word of line
                for i in splitted:
                    try: 
                        value = float(i)
                        if value>0: return splitted[0]
                    except ValueError: None
            return 'No failure detected!' 

        def getElement(lines):
            for line in lines:
                if 'failed element' in line:
                    el = [float(i) for i in line.split() if not i.isalpha()][0]
                    return el
            return '-'

        time = getTime(lines)
        loadfactor_eta = self.parameters['start_eta'] + (time-2)*self.parameters['stepsize_P']

        self.failureInfo = Munch(**{
            'eta':          loadfactor_eta,
            'failure_mode': getFailureMode(lines),
            'failure_element': getElement(lines),
        })
        
        return self


    def openPlot(self, type: str = 'v'):
        """
        Open the pdf with the corresponding plot

        args:   
        - type: str = 'v', 'm', 'c', 's', 'n', 'v_detail'
        """
        path = os.path.join(self.directory, self._figurepath)

        # types
        if type=='v': substring = 'v_check'
        elif type=='m': substring = 'm'
        elif type=='c': substring = 'sig_c3'
        elif type=='s': substring = 'sig_sr'
        elif type=='n': substring = 'n'
        elif type=='v_detail': substring = 'v'
        else: raise ValueError(f'{type} is not a valid input!')

        figure = os.path.join(path, f'{self.name}_{substring}.pdf')
        os.startfile(figure)

        return

    def openAllPlots(self):
        for type in ['v', 'm', 'c', 's', 'n', 'v_detail']:
            self.openPlot(type)
        return
        








# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------
# LOAD MULTIPLE SIMULATIONS
# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------

class LoadFolder(object):
    """
    Class to load multiple simulation results. Ability to load all or a subset of simulations in a specified folder.
    For each simulation, the result files are read and the most relevant information is organized in a pandas DF.
    Useful for probabilistic postprocessing & comparison.

    """

    def __init__(self) -> None:
        pass

    def load(self, folder, sim_list=None):
        """
        args:   
        - folder: str = directory where sims are located
        - sim_list: list = subset of simulations in the folder
        """
        if sim_list is None:
            sim_list = os.listdir(folder)
        else:
            sim_list = [i for i in sim_list]
            if type(sim_list[0])!=str:
                sim_list = self.simList(sim_list)
        self.sims = [os.path.join(folder, i) for i in sim_list]
        
        self.sims = [i for i in self.sims if os.path.exists(i)] # limit to existing simulations
        self.results = [ResultReader(path).readAll() for path in self.sims] # Read files for each simulation

        return self

    def stillRunning(self, folder, sim_list=None):
        """
        Only load information available at runtime.
        Good to track the progress of a series of simulations.
        """
        if sim_list is None:
            sim_list = os.listdir(folder)
        else:
            sim_list = [i for i in sim_list]
            if type(sim_list[0])!=str:
                sim_list = self.simList(sim_list)
        self.sims = [os.path.join(folder, i) for i in sim_list]

        self.results = []
        for path in self.sims:
            r = ResultReader(path)
            r.readParameterFile()
            try:
                r.getFailure()
            except FileNotFoundError: 
                r.failureInfo = {
                    'eta':          '-',
                    'failure_mode': '-',
                    'failure_element': '-'
                }
            self.results.append(r)

        return self


    @property
    def info(self):
        """
        Basic information such as eta, failure mode & material parameters.
        """
        lst = []
        for i in self.results:
            d = {'name': i.name} | i.failureInfo | i.parameters
            lst.append(d)

        df = pd.DataFrame.from_records(lst)

        return df
    
    @property
    def shear(self):
        lst = []
        for i in self.results:
            d = {'name': i.name} | {field.name: getattr(i.critElement, field.name) for field in fields(i.critElement)}
            lst.append(d)
        df = pd.DataFrame.from_records(lst)

        return df

    @property
    def df(self):
        combined = pd.concat([self.info, self.shear.drop('name', axis=1)], axis=1)
        return combined.loc[:,~combined.columns.duplicated()] 

    def simList(self, lst):
        return [f'sim_{i:03d}' for i in lst]

    # Convenience properties
    @property
    def coords(self):
        return self.results[0].mvn.coords[:, :2]

    @property
    def coords_steel(self):
        return self.results[0].steel.top_y.coords

    @property
    def coords_conc(self):
        return self.results[0].conc.mid.coords



# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------
# LOAD HISTORY OF A SIMULATION
# -----------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------

class LoadHistory:
    """
    Class to load the load path of a single simulation.
    To work, the .txt files need to be available for all timesteps.
        --> Use the member saveStepData() or the method PyCMM.utilities.saveAllSteps() before load()

    """
    def __init__(self, directory, name) -> None:
        self.directory = directory
        self.name = name
        self.path = os.path.join(directory, name)

        try:
            f = open(os.path.join(self.path, 'steps.txt'))
            f.close()
        except FileNotFoundError:
            raise FileNotFoundError("Steps.txt doesn't exist here. Run the saveStepData routine")

    def saveStepData(self):
        ...


    def load(self, filename: str = 'steps.txt'):
        """
        Load the results for all steps.
        """
        
        self.steps = utilities.loadSteps(os.path.join(self.path, filename))
        hist: List[ResultReader] = []

        for i, _ in enumerate(self.steps):
            r = ResultReader(self.path)
            r.plate_name = f's{i:02d}0Platte'
            r.readAll()
            hist.append(r)

        self.history = hist
        return self

    def __call__(self, *args, **kwds):
        return self.history[0]

    def data(
        self, stp_small: float = 0.025, stp_big: float = 0.10, 
        step_deadloads: int = 1, step_initdeformation: int = -1, step_liveloads_init: int = 2
        ):
        """
        Load path information. The load is backcalculated from the time and the chosen timestepping.
        """

        if hasattr(self, 'df'): 
            return self._df

        eta = self.history[0].failureInfo.eta
        start_eta = self.history[0].parameters.start_eta
        df          = pd.DataFrame.from_records(self.steps)
        fixer = 0 # Add in steps of 4 to correct the amount of 0.025 and 0.1 steps
        while True:
          
            diff        = (eta*10 % 1) - (start_eta*10 % 1)
            diff        = diff + 1 if diff<0 else diff
            nb_small    = int(diff / stp_small / 10) + fixer
            nb_big      = int(self.steps[-1]['time_n'] - 2 - nb_small)
            incs        = np.hstack(([stp_small]*nb_small, [stp_big]*nb_big))

            df['time'] = df.time_n - 1
            inc = np.ones(df.time.shape)
            inc[:incs.size] = incs
            df['inc'] = inc[::-1]
            df['factor'] = 1; 
            df.loc[df.step==step_initdeformation, ('factor')] = 0
            df.loc[df.step==step_deadloads, ('factor')] = 0
            df.loc[df.step==step_liveloads_init, ('factor')] = start_eta
            df['delta_load'] = np.diff(df.time.to_numpy(), prepend=-1) * df.inc * df.factor
            df['load'] = df.delta_load.cumsum()

            if abs(df.load.iloc[-1]-eta)<1e-7: break # Check whether the load of last step is equal to eta_sim
            else: fixer += int(stp_big/stp_small)

        self._df = df
        return self._df

    @property
    def info(self):
        """Failure information of the simulation."""
        i = self.history[0]
        d = {'name': i.name} | i.failureInfo | i.parameters
        return pd.DataFrame.from_records([d])
        

