# PyCMM

Example.ipynb should give a good overview.

Python toolbox for handling ANSYS models.
Developed during master thesis. Used on the HERO overpass model.

## Install

Add the path of PyCMM to the PYTHONPATH or store the package in the Python specific location directly.

## Requires

- Python > 3.9
- numpy
- scipy
- pandas
- matplotlib