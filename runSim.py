from PyCMM.batch_executer import SingleCall
import sys


if __name__=='__main__':

    if len(sys.argv)<2:
        raise ValueError('Usage: py runSim.py SIMNAME (folder=FOLDER) (delete=True or False)')
    
    simname = sys.argv[1] # name
    kwargs  = dict(arg.split('=') for arg in sys.argv[2:]) # folder, delete
    folder  = kwargs.get('folder', 'sim_inputs')
    delete  = kwargs.get('delete', False)

    SingleCall((simname, folder, delete))
    