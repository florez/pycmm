from PyCMM.batch_executer import runAll
import sys, os

if __name__=='__main__':
    kwargs = dict(arg.split('=') for arg in sys.argv[1:])
    folder = kwargs.get('folder', 'sim_inputs')
    delete = kwargs.get('delete', False)
    series = kwargs.get('series', None)
    parallel = int(kwargs.get('parallel', 3))

    if series==None:

        runAll(
            folder=folder, 
            parallel=parallel, 
            delete=delete,
            # tasks=['residual_000'],
        )

    else:
        tasks = [s for s in os.listdir(folder) if series in s]
        runAll(
            folder=folder, 
            parallel=parallel, 
            delete=delete,
            tasks=tasks,
        )
